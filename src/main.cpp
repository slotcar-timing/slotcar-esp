#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <MQTT.h>

// Network Definitions
const char* ssid = "....";
const char* password = "...";

// Replace this with zeroconf sometime!
const char* broker_ip = "192.168.178";

String mqttClientId = "";

WiFiClient wifiClient;
MQTTClient mqttClient;

// Put these in an array at some point. together with all the other related definitions
const int sensor1Gpio = 12;
const int sensor2Gpio = 14;

ICACHE_RAM_ATTR void beam1_triggered() {
  Serial.println("BEAM 1 BROKEN!!!");
}

ICACHE_RAM_ATTR void beam2_triggered() {
  Serial.println("BEAM 2 BROKEN!!!");
}

void setup_wifi() {
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void connect_mqtt() {
  boolean reconnectWifi = false;
  if (WiFi.status() != WL_CONNECTED) {
    Serial.print("WiFi not connected. Waiting for WiFi...");
    reconnectWifi = true;
  }
  while(WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  if(reconnectWifi) {
    Serial.println("WiFi is back up!");
  }

  Serial.print("Connecting to MQTT Broker...");
  // connect("host", "uname", "pwd")
  while (!mqttClient.connect(mqttClientId.c_str())) {

  }
}

void setup() {
  // basics
  pinMode(BUILTIN_LED, OUTPUT);
  Serial.begin(115200);

  // wifi
  setup_wifi();

  // build a client name that persists connection losses
  randomSeed(micros()); // has to have some randomness. Wifi flukes provide that.
  mqttClientId = "SlotcarTimer-";
  mqttClientId += String(random(0xffff), HEX);

  attachInterrupt(digitalPinToInterrupt(sensor1Gpio), beam1_triggered, FALLING);
  attachInterrupt(digitalPinToInterrupt(sensor2Gpio), beam2_triggered, FALLING);
}

void loop() {
  if (!mqttClient.connected()) {
    connect_mqtt();
  }
  mqttClient.loop();

  // put your main code here, to run repeatedly:
}
